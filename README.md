# Wood Problem #
This project shows how to use Optaplanner to solve an everyday problem: Finding how much wood I'll need to build some boxes

### How do I get set up? ###

* Download this repository
* Run "mvn clean install" to compile all the sources
* Import project into Eclipse
* Run WoodProblemHelloWorld class to see outputs

### Contribution guidelines ###
Feel free to send us any pull requests you like :)

### Who do I talk to? ###
Find us at [http://www.jwoop.com](http://www.jwoop.com)
