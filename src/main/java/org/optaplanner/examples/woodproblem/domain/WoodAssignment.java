/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.woodproblem.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.buildin.hardsoft.HardSoftScoreDefinition;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.persistence.xstream.impl.score.XStreamScoreConverter;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

@PlanningSolution
@XStreamAlias("WoodAssignment")
public class WoodAssignment extends AbstractPersistable implements Solution<HardSoftScore> {

	private static final long serialVersionUID = 1L;

	private List<WoodBeam> beamList;

    private List<WoodMeasure> measureList;

    @XStreamConverter(value = XStreamScoreConverter.class, types = {HardSoftScoreDefinition.class})
    private HardSoftScore score;

    @ValueRangeProvider(id = "beamRange")
    public List<WoodBeam> getBeamList() {
        return beamList;
    }

    public void setBeamList(List<WoodBeam> beamList) {
        this.beamList = beamList;
    }

    @PlanningEntityCollectionProperty
    public List<WoodMeasure> getMeasureList() {
        return measureList;
    }

    public void setMeasureList(List<WoodMeasure> measureList) {
        this.measureList = measureList;
    }

    public HardSoftScore getScore() {
        return score;
    }

    public void setScore(HardSoftScore score) {
        this.score = score;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    public Collection<? extends Object> getProblemFacts() {
        List<Object> facts = new ArrayList<Object>();
        facts.addAll(beamList);
        // Do not add the planning entity's (measureList) because that will be done automatically
        return facts;
    }

	public Set<WoodBeam> getUsedBeams() {
		Set<WoodBeam> retval = new HashSet<WoodBeam>();
		for (WoodMeasure measure : measureList) {
			retval.add(measure.getBeam());
		}
		return retval;
	}

}
