package org.optaplanner.examples.woodproblem.app;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.examples.woodproblem.domain.WoodAssignment;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;
import org.optaplanner.examples.woodproblem.persistence.WoodProblemGenerator;

public class WoodProblemHelloWorld {

    public static void main(String[] args) {
        // Build the Solver
        SolverFactory solverFactory = SolverFactory.createFromXmlResource(
                "org/optaplanner/examples/woodproblem/solver/woodProblemSolverConfig.xml");
        solveForBeamsOfSize(solverFactory, 2440);
        solveForBeamsOfSize(solverFactory, 3050);
        solveForBeamsOfSize(solverFactory, 3660);
        solveForBeamsOfSize(solverFactory, 4270);
    }

	private static void solveForBeamsOfSize(SolverFactory solverFactory, int beamsLength) {
		Solver solver = solverFactory.buildSolver();

        // Load a problem with 400 computers and 1200 processes
        WoodAssignment unsolvedWoodAssignment = new WoodProblemGenerator().createWoodProblem(8, 1, beamsLength);
        WoodAssignment solvedWoodAssignment = null;
        // Solve the problem
        solver.solve(unsolvedWoodAssignment);
        solvedWoodAssignment = (WoodAssignment) solver.getBestSolution();
        //store how many beams we're using
        int beamsAmount = solvedWoodAssignment.getUsedBeams().size();
        // Display the result
        System.out.println("\nSolved woodProblem with 8 small boxes, 1 big one, and " 
        	+ beamsAmount + " beams of " + beamsLength + "mm:\n"
            + toDisplayString(solvedWoodAssignment));
	}

    public static String toDisplayString(WoodAssignment woodAssignment) {
        StringBuilder displayString = new StringBuilder();
        for (WoodMeasure measure : woodAssignment.getMeasureList()) {
            WoodBeam beam = measure.getBeam();
            displayString.append("  ").append(measure.getLabel()).append(" -> ")
                    .append(beam == null ? null : beam.getLabel()).append("\n");
        }
        return displayString.toString();
    }

}
