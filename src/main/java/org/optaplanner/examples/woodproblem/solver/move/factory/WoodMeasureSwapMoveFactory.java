package org.optaplanner.examples.woodproblem.solver.move.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.examples.woodproblem.domain.WoodAssignment;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;
import org.optaplanner.examples.woodproblem.solver.move.WoodMeasureSwapMove;

public class WoodMeasureSwapMoveFactory implements MoveListFactory<WoodAssignment> {

    public List<Move> createMoveList(WoodAssignment assignment) {
        List<WoodMeasure> measureList = assignment.getMeasureList();
        List<Move> moveList = new ArrayList<Move>();
        for (ListIterator<WoodMeasure> leftIt = measureList.listIterator(); leftIt.hasNext();) {
            WoodMeasure leftMeasure = leftIt.next();
            for (ListIterator<WoodMeasure> rightIt = measureList.listIterator(leftIt.nextIndex()); rightIt.hasNext();) {
                WoodMeasure rightMeasure = rightIt.next();
                moveList.add(new WoodMeasureSwapMove(leftMeasure, rightMeasure));
            }
        }
        return moveList;
    }

}
