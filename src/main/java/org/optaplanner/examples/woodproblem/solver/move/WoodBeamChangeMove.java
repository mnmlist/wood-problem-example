/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.woodproblem.solver.move;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;

public class WoodBeamChangeMove implements Move {

    private WoodMeasure measure;
    private WoodBeam toBeam;

    public WoodBeamChangeMove(WoodMeasure measure, WoodBeam toBeam) {
        this.measure = measure;
        this.toBeam = toBeam;
    }

    public boolean isMoveDoable(ScoreDirector scoreDirector) {
        return !ObjectUtils.equals(measure.getBeam(), toBeam);
    }

    public Move createUndoMove(ScoreDirector scoreDirector) {
        return new WoodBeamChangeMove(measure, measure.getBeam());
    }

    public void doMove(ScoreDirector scoreDirector) {
        WoodProblemMoveHelper.moveWoodBeam(scoreDirector, measure, toBeam);
    }

    public Collection<? extends Object> getPlanningEntities() {
        return Collections.singletonList(measure);
    }

    public Collection<? extends Object> getPlanningValues() {
        return Collections.singletonList(toBeam);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof WoodBeamChangeMove) {
            WoodBeamChangeMove other = (WoodBeamChangeMove) o;
            return new EqualsBuilder()
                    .append(measure, other.measure)
                    .append(toBeam, other.toBeam)
                    .isEquals();
        } else {
            return false;
        }
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(measure)
                .append(toBeam)
                .toHashCode();
    }

    public String toString() {
        return measure + " => " + toBeam;
    }

}
