/*
 * Copyright 2012 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.woodproblem.solver.score;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;
import org.optaplanner.examples.woodproblem.domain.WoodAssignment;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;

public class WoodProblemMapBasedEasyScoreCalculator implements EasyScoreCalculator<WoodAssignment> {

    public HardSoftScore calculateScore(WoodAssignment woodAssignment) {
        int beamListSize = woodAssignment.getBeamList().size();
        Map<WoodBeam, Integer> beamLengthUsageMap = new HashMap<WoodBeam, Integer>(beamListSize);
        for (WoodBeam computer : woodAssignment.getBeamList()) {
            beamLengthUsageMap.put(computer, 0);
        }
        Set<WoodBeam> usedBeamSet = new HashSet<WoodBeam>(beamListSize);

        visitMeasureList(beamLengthUsageMap, usedBeamSet, woodAssignment.getMeasureList());

        int hardScore = sumHardScore(beamLengthUsageMap);
        int softScore = sumSoftScore(usedBeamSet);

        return HardSoftScore.valueOf(hardScore, softScore);
    }

    private void visitMeasureList(Map<WoodBeam, Integer> beamLengthUsageMap,
            Set<WoodBeam> usedBeamSet, List<WoodMeasure> measureList) {
        // We loop through the processList only once for performance
        for (WoodMeasure process : measureList) {
            WoodBeam beam = process.getBeam();
            if (beam != null) {
                int beamLengthUsage = beamLengthUsageMap.get(beam) + process.getRequiredLength();
                beamLengthUsageMap.put(beam, beamLengthUsage);
                usedBeamSet.add(beam);
            }
        }
    }

    private int sumHardScore(Map<WoodBeam, Integer> beamLengthUsageMap) {
        int hardScore = 0;
        for (Map.Entry<WoodBeam, Integer> usageEntry : beamLengthUsageMap.entrySet()) {
            WoodBeam computer = usageEntry.getKey();
            int beamLengthAvailable = computer.getLength() - usageEntry.getValue();
            if (beamLengthAvailable < 0) {
                hardScore += beamLengthAvailable;
            }
        }
        return hardScore;
    }

    private int sumSoftScore(Set<WoodBeam> usedComputerSet) {
        int softScore = 0;
        softScore -= usedComputerSet.size();
        return softScore;
    }

}
