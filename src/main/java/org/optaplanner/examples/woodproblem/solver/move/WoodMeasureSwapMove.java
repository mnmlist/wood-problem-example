/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.woodproblem.solver.move;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;

public class WoodMeasureSwapMove implements Move {

    private WoodMeasure leftMeasure;
    private WoodMeasure rightMeasure;

    public WoodMeasureSwapMove(WoodMeasure leftMeasure, WoodMeasure rightMeasure) {
        this.leftMeasure = leftMeasure;
        this.rightMeasure = rightMeasure;
    }

    public boolean isMoveDoable(ScoreDirector scoreDirector) {
        return !ObjectUtils.equals(leftMeasure.getBeam(), rightMeasure.getBeam());
    }

    public Move createUndoMove(ScoreDirector scoreDirector) {
        return new WoodMeasureSwapMove(rightMeasure, leftMeasure);
    }

    public void doMove(ScoreDirector scoreDirector) {
        WoodBeam oldLeftBeam = leftMeasure.getBeam();
        WoodBeam oldRightBeam = rightMeasure.getBeam();
        WoodProblemMoveHelper.moveWoodBeam(scoreDirector, leftMeasure, oldRightBeam);
        WoodProblemMoveHelper.moveWoodBeam(scoreDirector, rightMeasure, oldLeftBeam);
    }

    public Collection<? extends Object> getPlanningEntities() {
        return Arrays.asList(leftMeasure, rightMeasure);
    }

    public Collection<? extends Object> getPlanningValues() {
        return Arrays.asList(leftMeasure.getBeam(), rightMeasure.getBeam());
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof WoodMeasureSwapMove) {
            WoodMeasureSwapMove other = (WoodMeasureSwapMove) o;
            return new EqualsBuilder()
                    .append(leftMeasure, other.leftMeasure)
                    .append(rightMeasure, other.rightMeasure)
                    .isEquals();
        } else {
            return false;
        }
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(leftMeasure)
                .append(rightMeasure)
                .toHashCode();
    }

    public String toString() {
        return leftMeasure + " <=> " + rightMeasure;
    }

}
