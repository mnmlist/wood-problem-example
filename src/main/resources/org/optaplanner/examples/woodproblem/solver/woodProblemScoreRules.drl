package org.optaplanner.examples.woodproblem.solver;
dialect "java"

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScoreHolder;
import org.optaplanner.examples.woodproblem.domain.WoodAssignment;
import org.optaplanner.examples.woodproblem.domain.WoodBeam;
import org.optaplanner.examples.woodproblem.domain.WoodMeasure;

global HardSoftScoreHolder scoreHolder;

rule "requiredLengthTotal"
    when
        $beam : WoodBeam($length : length)
        $requiredLengthTotal : Number(intValue > $length) from accumulate(
            WoodMeasure(
                beam == $beam,
                $requiredLength : requiredLength),
            sum($requiredLength)
        )
    then
        scoreHolder.addHardConstraintMatch(kcontext, $length - $requiredLengthTotal.intValue());
end

rule "beamCost"
    when
        $beam : WoodBeam()
        exists WoodMeasure(beam == $beam)
    then
        scoreHolder.addSoftConstraintMatch(kcontext, -1);
end
